#!/bin/bash

# export-pygments.sh --- Build themes for the Tempus themes Pygments repo
#
# Copyright (c) 2021  Anna (cybertailor) Vyalkova <cyber@sysrq.in>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Commentary:
#
# Builds content files from the tempus-themes-generator and sends them
# to the tempus-themes-pygments directory.  This utility is intended
# for project maintenance.

### Code:

# Variables for the operations
tempusdir="$HOME/Git/Projects/tempus-themes" # check this!
generator_dir="${tempusdir}-generator"
outputdir="${tempusdir}-pygments"
generator="${generator_dir}/tempus-themes-generator.sh"

# Define the array with all the available schemes
schemes=()
while IFS=  read -r -d $'\0' item; do
	schemes+=("${item##*/}")
done < <(find "${generator_dir}/schemes" -type f -print0)

# Create directories
mkdir -p "${outputdir}/tempus_themes"

# Create the setuptools files
cat <<- EOF > "${outputdir}/setup.cfg"
[metadata]
name = tempus-themes
version = 2.3.0.20220206
url = https://gitlab.com/protesilaos/tempus-themes-generator
description = Accessible themes for Pygments
keywords = pygments, colorscheme
author = Protesilaos Stavrou
author_email = info@protesilaos.com
license = GPLv3+
classifiers =
    Development Status :: 5 - Production/Stable
    Environment :: Plugins
    License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
    Operating System :: OS Independent
    Programming Language :: Python
    Programming Language :: Python :: 3
    Topic :: Software Development :: Libraries :: Python Modules
    Topic :: Text Processing

[options]
packages = tempus_themes
install_requires = pygments

[options.entry_points]
pygments.styles =
EOF

cat <<- EOF > "${outputdir}/setup.py"
#!/usr/bin/env python3
from setuptools import setup
setup()
EOF

cd "$generator_dir" || exit 1

# Build all files for each item in the array
for i in ${schemes[*]}; do
    "$generator" "$i" pygments > "${outputdir}/tempus_themes/${i}.py"
    echo "    tempus_${i} = tempus_themes.${i}:Tempus${i^}Style" >> "${outputdir}/setup.cfg"
    echo "Preparing to export Tempus $i Pygments files"
done
