#!/bin/bash

# export-gtksourceview4.sh --- Build themes for the Tempus themes Gtksourceview4 repo
#
# Copyright (c) 2018-2021  Protesilaos Stavrou <info@protesilaos.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Commentary:
#
# Builds content files from the tempus-themes-generator and sends them
# to the tempus-themes-gtksourceview4 directory.  This utility is
# intended for project maintenance.

### Code:

# Variables for the operations
tempusdir="$HOME/Git/Projects/tempus-themes" # check this!
generator_dir="$tempusdir-generator"
outputdir="$tempusdir-gtksourceview4"
generator="$generator_dir/tempus-themes-generator.sh"

# Define the array with all the available schemes
schemes=()
while IFS=  read -r -d $'\0' item; do
	schemes+=("${item##*/}")
done < <(find "$generator_dir/schemes" -type f -print0)

# Create the directory
mkdir -p "$outputdir"

cd "$generator_dir" || exit 1

# Build all files for each item in the array
for i in ${schemes[*]}; do
    "$generator" "$i" gtksourceview4 > "$outputdir/tempus_$i.xml"
    echo "Preparing to export Tempus $i GTK4 Source View files"
done
