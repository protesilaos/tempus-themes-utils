#!/bin/bash

# build-all.sh --- Build all items for the Tempus themes repo
#
# Copyright (c) 2018-2021  Protesilaos Stavrou <info@protesilaos.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Commentary:
#
# Builds content files from the tempus-themes-generator and sends them
# to the main tempus-themes directory.  This utility is intended for
# project maintenance.

### Code:

# Variables for the operations
tempusdir="$HOME/Git/Projects/tempus-themes" # check this!
generator_dir="$tempusdir-generator"
generator="$generator_dir/tempus-themes-generator.sh"

# Define the array with all the available schemes
schemes=()
while IFS=  read -r -d $'\0' item; do
	schemes+=("${item##*/}")
done < <(find "$generator_dir/schemes" -type f -print0)

# Define the array with all the available templates
templates=()
while IFS=  read -r -d $'\0' item; do
	templates+=("${item##*/}")
done < <(find "$generator_dir/templates" -type f -print0)

cd "$generator_dir" || exit 1

# Dynamically create the necessary directories
# File path: ~/tempus-themes/{template}/tempus_{scheme}.{file extension}
for i in ${templates[*]}; do
    mkdir -p "$tempusdir/$i"
done

# Build all files for each item in the array
for i in ${schemes[*]}
do
    "$generator" "$i" gtksourceview3 > "$tempusdir/gtksourceview3/tempus_$i.xml"
    echo "Preparing Tempus $i GTK3 Source View files"

    "$generator" "$i" gtksourceview4 > "$tempusdir/gtksourceview4/tempus_$i.xml"
    echo "Preparing Tempus $i GTK4 Source View files"

    "$generator" "$i" alacritty > "$tempusdir/alacritty/tempus_$i.yml"
    echo "Preparing Tempus $i Alacritty files"

    "$generator" "$i" highlight > "$tempusdir/highlight/tempus_$i.theme"
    echo "Preparing Tempus $i Highlight files"

    "$generator" "$i" kitty > "$tempusdir/kitty/tempus_$i.conf"
    echo "Preparing Tempus $i Kitty files"

    "$generator" "$i" konsole > "$tempusdir/konsole/tempus_$i.colorscheme"
    echo "Preparing Tempus $i Konsole terminal files"

    "$generator" "$i" pygments > "$tempusdir/pygments/tempus_$i.py"
    echo "Preparing Tempus $i Pygments files"

    "$generator" "$i" roxterm > "$tempusdir/roxterm/Tempus ${i^}"
    echo "Preparing Tempus ${i^} roxterm files"

    "$generator" "$i" st > "$tempusdir/st/tempus_$i.h"
    echo "Preparing Tempus $i st (simple terminal) files"

    "$generator" "$i" shell-variables > "$tempusdir/shell-variables/tempus_$i.sh"
    echo "Preparing Tempus $i shell variable files"

    "$generator" "$i" tilix > "$tempusdir/tilix/tempus_$i.json"
    echo "Preparing Tempus $i Tilix files"

    "$generator" "$i" urxvt > "$tempusdir/urxvt/tempus_$i.Xresources"
    echo "Preparing Tempus $i Rxvt-Unicode (URvxt) files"

    "$generator" "$i" vim > "$tempusdir/vim/tempus_${i}.vim"
    echo "Preparing Tempus $i Vim files"

    "$generator" "$i" xfce4-terminal > "$tempusdir/xfce4-terminal/tempus_$i.theme"
    echo "Preparing Tempus $i Xfce4 terminal files"

    "$generator" "$i" xcolors > "$tempusdir/xcolors/tempus_$i.Xcolors"
    echo "Preparing Tempus $i Xcolors files"

    "$generator" "$i" xterm > "$tempusdir/xterm/tempus_$i.Xresources"
    echo "Preparing Tempus $i Xterm files"

    "$generator" "$i" yaml > "$tempusdir/yaml/tempus_$i.yml"
    echo "Preparing Tempus $i YAML files"
done
