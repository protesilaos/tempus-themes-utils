# Utilities for maintaining the Tempus Themes

A collection of shell scripts to maintain the various git repositories
of the Tempus themes project:

* [Tempus themes **Alacritty**](https://gitlab.com/protesilaos/tempus-themes-alacritty)
* [Tempus themes **GTK3 Source View**](https://gitlab.com/protesilaos/tempus-themes-gtksourceview3)
* [Tempus themes **GTK4 Source View**](https://gitlab.com/protesilaos/tempus-themes-gtksourceview4)
* [Tempus themes **Highlight**](https://gitlab.com/protesilaos/tempus-themes-highlight)
* [Tempus themes **Kitty**](https://gitlab.com/protesilaos/tempus-themes-kitty)
* [Tempus themes **Konsole**](https://gitlab.com/protesilaos/tempus-themes-konsole)
* [Tempus themes **Pygments**](https://gitlab.com/protesilaos/tempus-themes-pygments)
* [Tempus themes **Roxterm**](https://gitlab.com/protesilaos/tempus-themes-roxterm)
* [Tempus themes **ST**](https://gitlab.com/protesilaos/tempus-themes-st)
* [Tempus themes **Tilix**](https://gitlab.com/protesilaos/tempus-themes-tilix)
* [Tempus themes **URxvt**](https://gitlab.com/protesilaos/tempus-themes-urxvt)
* [Tempus themes **Vim plugin**](https://gitlab.com/protesilaos/tempus-themes-vim)
* [Tempus themes **Xfce4 terminal**](https://gitlab.com/protesilaos/tempus-themes-xfce4-terminal)
* [Tempus themes **Xterm**](https://gitlab.com/protesilaos/tempus-themes-xterm)

## Contributing

Unless you want to improve this repo, all contributions should be
submitted to the Tempus themes generator.  See its
[CONTRIBUTING.md](https://gitlab.com/protesilaos/tempus-themes-generator/blob/master/CONTRIBUTING.md).

## COPYING

GNU General Public License Version 3.
