#!/bin/bash

# export-all.sh --- Run all Tempus themes exporter scripts
#
# Copyright (c) 2018-2021  Protesilaos Stavrou <info@protesilaos.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Commentary:
#
# Runs all specialised export-*.sh scripts in the present repo.  This
# utility is intended for project maintenance.

### Code:

[ "${PWD##*/}" == 'tempus-themes-utils' ] || { echo "ERROR. Must be run from the root of the 'tempus-themes-utils' directory." ; exit 1; }

export_scripts=()
while IFS=  read -r -d $'\0' item; do
	export_scripts+=("${item##*/}")
done < <(find . -name 'export-*' ! -name 'export-all.sh' -print0)

for i in ${export_scripts[*]}; do
    ./"$i"
done
